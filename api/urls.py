from __future__ import unicode_literals

from django.conf.urls import url, include
from rest_auth.views import LoginView
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view

from categories.views import CategoryViewSet, CategoryItemViewSet
from orders.views import OrderViewSet, OrderItemViewSet
from products.views import (ProductViewSet, ProductStockViewSet,
                            ProductPriceViewSet)
from restaurants.views import RestaurantViewSet
from users.views import UserViewSet, UserProfileViewSet

router = routers.SimpleRouter()
router.register(r'categories', CategoryViewSet)
router.register(r'category_items', CategoryItemViewSet)
router.register(r'orders', OrderViewSet)
router.register(r'order_items', OrderItemViewSet)
router.register(r'products', ProductViewSet)
router.register(r'product_prices', ProductPriceViewSet)
router.register(r'product_stocks', ProductStockViewSet)
router.register(r'restaurants', RestaurantViewSet)
router.register(r'users', UserViewSet)
router.register(r'user_profiles', UserProfileViewSet)

urlpatterns = [
    url(r'^v1/', include(router.urls)),
    url(r'^v1/auth/login/$', LoginView.as_view(),
        name='rest_login'),
    url('^docs/', get_swagger_view(title='Yemeksepeti API'),
        name='openapi-schema'),
]
