from enumfields import Enum


class GenderType(Enum):
    male = 'male'
    female = 'female'


class UserType(Enum):
    customer = 'customer'
    restaurant_owner = 'restaurant owner'
