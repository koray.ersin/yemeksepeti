from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.db import models
from enumfields import EnumField

from base.models import StarterModel
from users.enums import GenderType, UserType


class UserProfile(StarterModel):
    phone_message = 'Phone number must be entered in the format: 05999999999'

    phone_regex = RegexValidator(
        regex=r'^(05)\d{9}$',
        message=phone_message
    )
    phone_number = models.CharField(validators=[phone_regex], max_length=60,
                                    null=True, blank=True)
    user = models.OneToOneField(User, unique=True)
    birthdate = models.DateField(null=True, blank=True)
    gender = EnumField(GenderType, null=True, blank=True)
    user_type = EnumField(UserType, default=UserType.customer)

    def __str__(self):
        return '{} - {}'.format(self.user.first_name, self.user.last_name)

    @property
    def is_customer(self):
        return self.user_type == UserType.customer

    @property
    def is_restaurant_owner(self):
        return self.user_type == UserType.restaurant_owner
