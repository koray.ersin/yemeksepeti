from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from base.views import FilterAndOrderingMixin
from users.models import UserProfile
from users.serializers import UserSerializer, UserProfileSerializer


class AdminOrAuthenticatedPermissionMixin(FilterAndOrderingMixin):
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        user = self.request.user
        if getattr(user, 'is_superuser'):
            return super(
                AdminOrAuthenticatedPermissionMixin, self).get_queryset()
        else:
            return User.objects.filter(id=user.id)


class UserViewSet(AdminOrAuthenticatedPermissionMixin, viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserProfileViewSet(AdminOrAuthenticatedPermissionMixin,
                         viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer
