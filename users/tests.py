from model_mommy import mommy

from base.tests import BaseApiTestCase
from users.enums import GenderType
from users.models import UserProfile
from users.serializers import UserProfileSerializer


class UserProfileTestCase(BaseApiTestCase):
    endpoint = "/api/v1/user_profiles/"

    def setUp(self):
        self.create_user()

    def test_create_user_profile(self):
        user_profile = mommy.prepare(
            UserProfile, gender=GenderType.male, user=self.user
        )
        user_profile_serializer = UserProfileSerializer(user_profile)
        serializer = UserProfileSerializer(data=user_profile_serializer.data)
        self.assertTrue(serializer.is_valid(raise_exception=True))
        self.assertTrue(self.post_request_and_check_status(serializer.data))

    def test_update_user_profile(self):
        user_profile = mommy.make(
            UserProfile, gender=GenderType.male, user=self.user
        )
        data = {
            "phone_number": "05303334455"
        }
        self.assertTrue(
            self.patch_request_and_check_status(
                endpoint=self.endpoint + '{}/'.format(user_profile.id),
                data=data
            )
        )
        user_profile.refresh_from_db()

        self.assertEqual(user_profile.phone_number, data['phone_number'])
