from django.contrib.auth.models import User
from rest_framework import serializers

from base.fields import EnumField
from users.enums import GenderType, UserType
from users.models import UserProfile


class UserProfileSerializer(serializers.ModelSerializer):
    gender = EnumField(enum_type=GenderType)
    user_type = EnumField(enum_type=UserType)

    class Meta:
        model = UserProfile
        fields = ('pk', 'phone_number', 'birthdate', 'gender', 'user_type',
                  'user')


class UserSerializer(serializers.ModelSerializer):
    userprofile = UserProfileSerializer()

    class Meta:
        model = User
        fields = ('pk', 'username', 'first_name', 'last_name', 'email',
                  'is_staff', 'is_active', 'date_joined', 'last_login',
                  'is_superuser', 'userprofile', 'password')
