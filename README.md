**Projenin çalışması için;**

`docker-compose build` ardından `docker-compose start` yapmanız yeterli olacaktır.

Proje `http://0.0.0.0:8600/` endpoint'inden ayağa kalkacaktır.

dummy data için `docker-compose exec web python manage.py loaddata fixtures/fixtures.yaml` yapmanız yeterlidir.

İstenilen endpointler ise;

- Bir endpoint siparişi alacak ve pub/sub’a atacak 
  
URL: `http://127.0.0.1:8080/api/v1/orders/publish_order/`

dummy curl:
`curl --location --request POST 'http://127.0.0.1:8060/api/v1/orders/publish_order/' \
--header 'Authorization: Token f83f3cb804f796886f8a881ea975a4375c8af193' \
--header 'Content-Type: application/json' \
--data-raw '{
    "products":[
        {
            "product": 1,
            "count":2
        },
        {
            "product": 2,
            "count":1
        }
    ],
    "payment_type": "pay_on_delivery"
}'`


- Diğer bir endpoint oradan alıp siparişi tamamlayacak.
Bu aşamayı iki farklı endpoint'e böldüm. Birincisi listeleme kısmı, ikincisi ise accept veya decline edebilmek adına ayrı bir endpoint.
  
    Birinci kısım için, subscribe olduğu channel'dan gelen veriyi cache'e yazılıyor.

    İkinci kısım için, order number'a ve restorant id'sine göre orderları accept yada decline ediliyor. ve sipariş oluşturuluyor.

Birinci kısım URL: `http://0.0.0.0:8600/api/v1/orders/subscribe_order/`


dummy curl:
`curl --location --request GET 'http://0.0.0.0:8600/api/v1/orders/subscribe_order/' \
--header 'Authorization: Token db851b9ddcd39e3d7e6340b796e1af5f5769178f'`


Cache'deki verileri listelemek için URL: `http://0.0.0.0:8600/api/v1/orders/read_order_from_subscribed/`

dummy curl:
`curl --location --request GET 'http://0.0.0.0:8600/api/v1/orders/read_order_from_subscribed/' \
--header 'Authorization: Token db851b9ddcd39e3d7e6340b796e1af5f5769178f'`


İkinci kısım URL: `http://0.0.0.0:8600/api/v1/orders/accept_or_decline_subscribed_order/`


dummy curl:
`curl --location --request POST 'http://0.0.0.0:8600/api/v1/orders/accept_or_decline_subscribed_order/' \
--header 'Authorization: Token db851b9ddcd39e3d7e6340b796e1af5f5769178f' \
--header 'Content-Type: application/json' \
--data-raw '{
    "accept_or_decline":true,
    "order_number": "MAHSKLZWBWMBRSXOKYKE"
}'`


- 3. ve son endpoint siparişlerin listlenmesi.(bekleyenler? tamamlananlar?)
    
URL: `http://0.0.0.0:8600/api/v1/orders/`

    
dummy curl teslim edilenler için:
`curl --location --request GET 'http://0.0.0.0:8600/api/v1/orders/?status=delivered' \
--header 'Authorization: Token db851b9ddcd39e3d7e6340b796e1af5f5769178f'`
    
dummy curl bekleyenler için:
`curl --location --request GET 'http://0.0.0.0:8600/api/v1/orders/?status=waiting' \
--header 'Authorization: Token db851b9ddcd39e3d7e6340b796e1af5f5769178f'`

Test için `docker-compose exec web python manage.py test`

Login olabilmek için `http://0.0.0.0:8600/api/v1/auth/login/` (tüm kullanıcı şifreleri 123qweasd olarak tanımlanmıştır.)

Döküman için `http://0.0.0.0:8600/api/docs/`