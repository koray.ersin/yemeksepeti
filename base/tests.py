from django.contrib.auth.models import User
from model_mommy import mommy
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase


class BaseApiTestCase(APITestCase):
    endpoint = None

    def create_user(self):
        self.user = mommy.make(User,
                               is_superuser=True,
                               username='test',
                               is_active=True,
                               is_staff=True)
        self.token = mommy.make(Token, user=self.user)
        self.client.credentials(
            HTTP_AUTHORIZATION='Token {}'.format(self.token.key)
        )

    def post_request_and_check_status(self, data):
        response = self.client.post(self.endpoint, data, format='json')
        return status.HTTP_201_CREATED == response.status_code

    def patch_request_and_check_status(self, endpoint, data):
        response = self.client.patch(endpoint, data, format='json')
        return status.HTTP_200_OK == response.status_code
