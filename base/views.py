from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.permissions import IsAdminUser


class FilterAndOrderingMixin(object):
    filter_backends = [DjangoFilterBackend]
    http_method_names = ['get', 'post', 'put', 'patch', 'options']
    filter_fields = '__all__'
    ordering_fields = '__all__'


class AdminOrReadonlyPermissionMixin(FilterAndOrderingMixin):

    def get_permissions(self):
        if not self.request.method == 'GET':
            self.permission_classes = [IsAdminUser, ]

        return super(AdminOrReadonlyPermissionMixin, self).get_permissions()
