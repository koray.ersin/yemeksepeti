from rest_framework import viewsets

from base.views import AdminOrReadonlyPermissionMixin
from categories.models import Category, CategoryItem
from categories.serializers import CategorySerializer, CategoryItemSerializer


class CategoryViewSet(AdminOrReadonlyPermissionMixin, viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class CategoryItemViewSet(AdminOrReadonlyPermissionMixin,
                          viewsets.ModelViewSet):
    queryset = CategoryItem.objects.all()
    serializer_class = CategoryItemSerializer
