from model_mommy import mommy

from base.tests import BaseApiTestCase
from categories.models import Category, CategoryItem
from categories.serializers import CategorySerializer, CategoryItemSerializer
from products.models import Product


class CategoryTestCase(BaseApiTestCase):
    endpoint = "/api/v1/categories/"

    def setUp(self):
        self.create_user()
        self.category = mommy.make(Category, name='hamburger')

    def test_create_category(self):
        category = mommy.prepare(Category, name='pizza')
        category_serializer = CategorySerializer(category)
        serializer = CategorySerializer(data=category_serializer.data)
        self.assertTrue(serializer.is_valid())
        self.assertTrue(self.post_request_and_check_status(serializer.data))

    def test_update_category(self):
        data = {
            "name": "Hamburger"
        }

        self.assertTrue(
            self.patch_request_and_check_status(
                endpoint=self.endpoint + '{}/'.format(self.category.id),
                data=data
            )
        )

        self.category.refresh_from_db()
        self.assertEqual(self.category.name, data['name'])


class CategoryItemTestCase(BaseApiTestCase):
    endpoint = "/api/v1/category_items/"

    def setUp(self):
        self.create_user()
        self.category = mommy.make(Category, name='pizza')
        self.product_1 = mommy.make(Product, name='margarita pizza')
        self.product_2 = mommy.make(Product, name='chicago pizza')
        self.category_item_1 = mommy.make(CategoryItem, category=self.category,
                                          product=self.product_2)

    def test_create_category_item(self):
        category_item_1 = mommy.prepare(CategoryItem, category=self.category,
                                        product=self.product_1)
        category_item_1_serializer = CategoryItemSerializer(category_item_1)
        serializer = CategoryItemSerializer(
            data=category_item_1_serializer.data)
        self.assertTrue(serializer.is_valid())

        self.assertTrue(self.post_request_and_check_status(serializer.data))

    def test_update_category_item(self):
        data = {
            "product": self.product_2.id
        }

        self.assertTrue(
            self.patch_request_and_check_status(
                endpoint=self.endpoint + '{}/'.format(self.category_item_1.id),
                data=data
            )
        )
        self.category_item_1.refresh_from_db()

        self.assertEqual(self.category_item_1.product.id, data['product'])
