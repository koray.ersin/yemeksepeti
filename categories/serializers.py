from rest_framework import serializers

from categories.models import Category, CategoryItem


class CategoryItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoryItem
        fields = ('pk', 'category', 'product')


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('pk', 'name', 'categoryitem_set')
