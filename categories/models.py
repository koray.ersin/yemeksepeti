from django.db import models

from base.models import StarterModel
from products.models import Product


class Category(StarterModel):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class CategoryItem(StarterModel):
    product = models.ForeignKey(Product)
    category = models.ForeignKey(Category)

    def __str__(self):
        return '{} - {}'.format(self.product.name, self.category.name)
