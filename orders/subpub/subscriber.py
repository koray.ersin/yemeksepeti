import json

import redis
from django.conf import settings
from django.core.cache import cache


class Subscriber(object):
    def __init__(self, restaurant_id):
        self.client = redis.Redis(host=self.__redis_host, port=6379)
        self.pub_sub = self.client.pubsub()
        self.pub_sub.subscribe(self.__channel(restaurant_id))
        self.restaurant_id = restaurant_id

    @property
    def __redis_host(self):
        return getattr(settings, 'REDIS_HOST', '127.0.0.1')

    def __channel(self, restaurant_id):
        channel = getattr(
            settings, 'REDIS_PUB_SUB_CHANNEL', "yemeksepeti_order_channel_{}")
        channel = channel.format(restaurant_id)
        return channel

    def __get_cache_key(self, order_number):
        cache_key = "restaurant_id:{}order_number:{}".format(self.restaurant_id,
                                                             order_number)
        return cache_key

    @property
    def __get_restaurant_key(self):
        return "restaurant_id:{}".format(self.restaurant_id)

    def __add_order_number(self, order_number):
        if cache.get(self.__get_restaurant_key):
            order_numbers = cache.get(self.__get_restaurant_key)
            order_numbers.append(order_number)
            cache.delete(self.__get_restaurant_key)
            cache.add(
                self.__get_restaurant_key,
                order_numbers,
                timeout=None

            )
        else:
            cache.add(
                self.__get_restaurant_key,
                [order_number, ],
                timeout=None
            )

    @property
    def __order_numbers(self):
        return cache.get(self.__get_restaurant_key, [])

    @property
    def orders(self):
        data = []
        for order_number in self.__order_numbers:
            data.append(self.get_subscribed_order(order_number))
        return data

    def subscribe(self):
        message = json.loads(self.__subscribe)
        order_number = message.get('order_number')
        cache.add(
            self.__get_cache_key(order_number),
            message,
            timeout=None
        )
        self.__add_order_number(order_number)
        return message

    def get_subscribed_order(self, order_number):
        return cache.get(self.__get_cache_key(order_number=order_number))

    def delete_subscribed_order(self, order_number):
        return cache.delete(self.__get_cache_key(order_number=order_number))

    @property
    def __subscribe(self):
        while True:
            message = self.pub_sub.get_message()
            if message and not message['data'] == 1:
                return message['data'].decode('utf-8')
