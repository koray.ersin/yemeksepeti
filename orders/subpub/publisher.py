import json

import redis
from django.conf import settings


class Publisher(object):
    def __init__(self):
        self.client = redis.Redis(host=self.__redis_host, port=6379)

    @property
    def __redis_host(self):
        return getattr(settings, 'REDIS_HOST', '127.0.0.1')

    def __channel(self, restaurant_id):
        channel = getattr(
            settings, 'REDIS_PUB_SUB_CHANNEL', "yemeksepeti_order_channel_{}")
        channel = channel.format(restaurant_id)
        return channel

    def publish(self, data: dict):
        self.client.publish(
            self.__channel(data.get('restaurant_id')),
            json.dumps(data)
        )
        return data
