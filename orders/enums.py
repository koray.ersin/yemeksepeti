from enumfields import Enum


class PaymentType(str, Enum):
    pay_on_delivery = 'pay_on_delivery'


class OrderStatus(str, Enum):
    waiting = "waiting"
    delivered = "delivered"
    on_the_way = "on_the_way"
    cancelled = "cancelled"
