from orders.models import Order, OrderItem
from orders.serializers import OrderSerializer, OrderItemSerializer
from django.db.transaction import atomic

"""
{
    "products": [
        {
            "product_id": 1,
            "count": 2
        },
        {
            "product_id": 2,
            "count": 1
        }
    ],
    "payment_type": "pay_on_delivery",
    "number": "AKENAQLOOWZJMJPJXGTK",
    "restaurant_id": 1,
    "customer_id": 7
}
"""
class OrderService(object):
    @atomic
    def generate_order(self, data):
        pass


