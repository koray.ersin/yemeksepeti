import json
from decimal import Decimal

import mock
from django.contrib.auth.models import User
from django.core.cache import cache
from model_mommy import mommy
from rest_framework import status

from base.tests import BaseApiTestCase
from orders.enums import PaymentType, OrderStatus
from orders.models import Order, OrderItem
from orders.serializers import OrderSerializer, OrderItemSerializer
from products.models import Product, ProductStock, ProductPrice
from restaurants.models import Restaurant


class OrderItemTestCase(BaseApiTestCase):
    endpoint = "/api/v1/order_items/"

    def setUp(self):
        self.create_user()
        self.product_1 = mommy.make(Product, name='margarita pizza')
        self.product_1_price = mommy.make(
            ProductPrice,
            price=Decimal(20),
            product=self.product_1
        )
        self.product_1_stock = mommy.make(
            ProductStock,
            stock=5,
            product=self.product_1
        )

        self.product_2 = mommy.make(Product, name='pepperoni pizza')
        self.product_2_price = mommy.make(
            ProductPrice,
            price=Decimal(30),
            product=self.product_2
        )
        self.product_2_stock = mommy.make(
            ProductStock,
            stock=8,
            product=self.product_2
        )

        self.order = mommy.make(Order)

    def test_create_order_item(self):
        order_item_1 = mommy.prepare(
            OrderItem, product=self.product_1, order=self.order,
            price=self.product_1.active_price)
        order_item_1_serializer = OrderItemSerializer(order_item_1)
        serializer = OrderItemSerializer(
            data=order_item_1_serializer.data)
        self.assertTrue(serializer.is_valid())

        self.assertTrue(self.post_request_and_check_status(serializer.data))

        order_item_1 = OrderItem.objects.get(product=self.product_1)

        self.assertIsNotNone(order_item_1.id)
        self.assertEqual(order_item_1.price, self.product_1.active_price)
        self.assertEqual(self.order.total_amount, self.product_1.active_price)

    def test_update_order_item(self):
        order_item_1 = mommy.make(
            OrderItem, product=self.product_1, order=self.order,
            price=self.product_1.active_price)
        data = {
            "product": self.product_2.id
        }

        self.assertTrue(
            self.patch_request_and_check_status(
                endpoint=self.endpoint + '{}/'.format(order_item_1.id),
                data=data
            )
        )
        order_item_1.refresh_from_db()

        self.assertEqual(order_item_1.product.id, data['product'])
        self.assertEqual(order_item_1.price, self.product_2.active_price)
        self.assertEqual(self.order.total_amount, self.product_2.active_price)


class OrderTestCase(BaseApiTestCase):
    endpoint = "/api/v1/orders/"

    def setUp(self):
        self.create_user()

        self.customer = mommy.make(
            User, first_name='Lorem', last_name='Ipsum', username='lorem.ipsum')
        self.restaurant = mommy.make(Restaurant, user=self.user)

    def test_create_order(self):
        order_data = {
            'customer': self.customer.id,
            'payment_type': PaymentType.pay_on_delivery,
            'restaurant': self.restaurant.id
        }
        serializer = OrderSerializer(data=order_data)
        self.assertTrue(serializer.is_valid(raise_exception=True))

        self.assertTrue(self.post_request_and_check_status(order_data))

    def test_update_order(self):
        order = mommy.make(Order, customer=self.customer,
                           payment_type=PaymentType.pay_on_delivery,
                           restaurant=self.restaurant)
        data = {
            'status': OrderStatus.on_the_way,
        }

        self.assertTrue(
            self.patch_request_and_check_status(
                endpoint=self.endpoint + '{}/'.format(order.id),
                data=data
            )
        )
        order.refresh_from_db()

        self.assertTrue(order.is_send)
        self.assertIsNone(order.delivered_date)
        self.assertEqual(order.status, OrderStatus.on_the_way)

        data = {
            'status': OrderStatus.delivered,
        }

        self.assertTrue(
            self.patch_request_and_check_status(
                endpoint=self.endpoint + '{}/'.format(order.id),
                data=data
            )
        )
        order.refresh_from_db()

        self.assertTrue(order.is_send)
        self.assertIsNotNone(order.delivered_date)
        self.assertEqual(order.status, OrderStatus.delivered)


class PublishOrderTestCase(BaseApiTestCase):
    endpoint = "/api/v1/orders/publish_order/"

    def setUp(self):
        self.create_user()
        restaurant = mommy.make(Restaurant)
        product_1 = mommy.make(Product, restaurant=restaurant)
        product_2 = mommy.make(Product, restaurant=restaurant)
        self.request_data = {
            "products": [
                {
                    "product_id": product_1.id,
                    "count": 2
                },
                {
                    "product_id": product_2.id,
                    "count": 1
                }
            ],
            "payment_type": "pay_on_delivery"
        }

    def test_publish_order(self):
        self.assertTrue(self.post_request_and_check_status(self.request_data))


class SubscribeOrderTestCase(BaseApiTestCase):
    endpoint = "/api/v1/orders/subscribe_order/"
    publish_endpoint = "/api/v1/orders/publish_order/"

    def setUp(self):
        cache.clear()
        self.create_user()
        restaurant = mommy.make(Restaurant, user=self.user)
        product_1 = mommy.make(Product, restaurant=restaurant)
        mommy.make(
            ProductPrice,
            price=Decimal(20),
            product=product_1
        )
        self.product_1_stock = mommy.make(
            ProductStock,
            stock=5,
            product=product_1
        )

        product_2 = mommy.make(Product, restaurant=restaurant)
        mommy.make(
            ProductPrice,
            price=Decimal(20),
            product=product_2
        )
        mommy.make(
            ProductStock,
            stock=5,
            product=product_2
        )

        self.request_data = {
            "products": [
                {
                    "product_id": product_1.id,
                    "count": 2
                },
                {
                    "product_id": product_2.id,
                    "count": 1
                }
            ],
            "payment_type": "pay_on_delivery"
        }

        response = self.client.post(self.publish_endpoint, self.request_data,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.mocked_data = response.data

    @mock.patch('redis.client.PubSub.get_message')
    def test_subscribe_order(self, m):
        m.return_value = {'data': json.dumps(self.mocked_data).encode('utf-8')}
        response = self.client.get(self.endpoint)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(
            "/api/v1/orders/read_order_from_subscribed/")

        self.assertDictEqual(response.data[0], self.mocked_data)

    @mock.patch('redis.client.PubSub.get_message')
    def test_accept_subscribed_order(self, m):
        m.return_value = {'data': json.dumps(self.mocked_data).encode('utf-8')}

        response = self.client.get(self.endpoint)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.post(
            "/api/v1/orders/accept_or_decline_subscribed_order/", data={
                "accept_or_decline": True,
                "order_number": self.mocked_data.get('order_number')
            })

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        stock = self.product_1_stock.stock
        self.product_1_stock.refresh_from_db()
        self.assertNotEqual(stock, self.product_1_stock.stock)
        self.assertEqual(stock, self.product_1_stock.stock + 2)
