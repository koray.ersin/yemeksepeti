from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import list_route
from rest_framework.response import Response

from base.views import AdminOrReadonlyPermissionMixin, FilterAndOrderingMixin
from orders.models import Order, OrderItem
from orders.serializers import (OrderSerializer,
                                OrderItemSerializer,
                                PublishOrderSerializer,
                                AcceptOrDeclineSubscribedOrderSerializer,
                                SubscribedOrderSerializer)
from orders.subpub.publisher import Publisher
from orders.subpub.subscriber import Subscriber
from restaurants.models import Restaurant


class OrderViewSet(FilterAndOrderingMixin, viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    publisher = Publisher()

    @list_route(methods=['post'])
    def publish_order(self, request, *args, **kwargs):
        serializer = PublishOrderSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        data = self.publisher.publish(
            data=serializer.custom_data(request=request))
        return Response(status=status.HTTP_201_CREATED, data=data)

    @list_route(methods=['get'])
    def subscribe_order(self, request, *args, **kwargs):
        try:
            restaurant = Restaurant.objects.get(user=request.user)
            subscriber = Subscriber(restaurant.id)
            data = subscriber.subscribe()
            return Response(status=status.HTTP_200_OK, data=data)
        except Restaurant.DoesNotExist:
            return Response(status=status.HTTP_403_FORBIDDEN)

    @list_route(methods=['post'])
    def accept_or_decline_subscribed_order(self, request, *args, **kwargs):
        try:
            restaurant = Restaurant.objects.get(user=request.user)
            serializer = AcceptOrDeclineSubscribedOrderSerializer(
                data=request.data)
            serializer.is_valid(raise_exception=True)
            order_number = serializer.data.get('order_number')
            subscriber = Subscriber(restaurant.id)
            data = subscriber.get_subscribed_order(order_number)

            serializer = SubscribedOrderSerializer(data=data)
            serializer.is_valid(raise_exception=True)
            order = serializer.generate(serializer.validated_data)

            serializer = self.serializer_class(instance=order)
            subscriber.delete_subscribed_order(order_number)
            return Response(status=status.HTTP_201_CREATED,
                            data=serializer.data)
        except Restaurant.DoesNotExist:
            return Response(status=status.HTTP_403_FORBIDDEN)

    @list_route(methods=['get'])
    def read_order_from_subscribed(self, request, *args, **kwargs):
        try:
            restaurant = Restaurant.objects.get(user=request.user)
            subscriber = Subscriber(restaurant.id)
            data = subscriber.orders
            return Response(status=status.HTTP_200_OK, data=data)
        except Restaurant.DoesNotExist:
            return Response(status=status.HTTP_403_FORBIDDEN)


class OrderItemViewSet(AdminOrReadonlyPermissionMixin, viewsets.ModelViewSet):
    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializer
