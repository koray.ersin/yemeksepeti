import random
import string

from django.contrib.auth.models import User
from django.db import models
from enumfields import EnumField

from base.models import StarterModel
from orders.enums import PaymentType, OrderStatus
from products.models import Product
from restaurants.models import Restaurant


class Order(StarterModel):
    number = models.CharField(max_length=128)
    customer = models.ForeignKey(User)
    payment_type = EnumField(PaymentType, null=True, blank=True, max_length=16)
    delivered_date = models.DateTimeField(null=True, blank=True)
    is_send = models.BooleanField(default=False)
    status = EnumField(OrderStatus, default=OrderStatus.waiting)
    restaurant = models.ForeignKey(Restaurant)

    def __str__(self):
        return self.number

    @property
    def total_amount(self):
        return sum(
            [order_item.price for order_item in self.orderitem_set.all()]
        )

    @staticmethod
    def generate_number():
        return ''.join(
            (random.choice(string.ascii_uppercase) for x in range(20)))


class OrderItem(StarterModel):
    product = models.ForeignKey(Product)
    price = models.DecimalField(max_digits=12, decimal_places=2)
    order = models.ForeignKey(Order)

    def __str__(self):
        return '{} - {}'.format(self.product.name, self.order.number)
