from django.contrib.auth.models import User
from django.db.transaction import atomic
from django.utils import timezone
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from base.fields import EnumField
from orders.enums import OrderStatus, PaymentType
from orders.models import Order, OrderItem
from products.models import Product, ProductStock
from restaurants.models import Restaurant


class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = ('pk', 'created_date', 'modified_date', 'product', 'price',
                  'order')
        read_only_fields = ('created_date', 'modified_date', 'price')

    @staticmethod
    def set_product_price(validated_data):
        product = validated_data.get('product')
        active_price = product.active_price
        if active_price:
            validated_data['price'] = active_price
        else:
            raise ValueError("{} - this product cannot buy.".format(
                product.name
            ))
        return validated_data

    def create(self, validated_data):
        validated_data = self.set_product_price(validated_data)

        return super(OrderItemSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        if validated_data.get('product'):
            validated_data = self.set_product_price(validated_data)

        return super(OrderItemSerializer, self).update(instance, validated_data)


class OrderSerializer(serializers.ModelSerializer):
    orderitem_set = OrderItemSerializer(many=True, read_only=True)
    payment_type = EnumField(enum_type=PaymentType, allow_null=True,
                             allow_blank=True)
    status = EnumField(enum_type=OrderStatus, default=OrderStatus.waiting)

    class Meta:
        model = Order
        fields = ('pk', 'created_date', 'modified_date', 'number', 'customer',
                  'payment_type', 'delivered_date', 'is_send', 'status',
                  'orderitem_set', 'restaurant')
        read_only_fields = ('created_date', 'modified_date', 'number',
                            'delivered_date', 'is_send')

    def create(self, validated_data):
        validated_data['number'] = Order.generate_number()

        return super(OrderSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        if validated_data['status'] in [OrderStatus.on_the_way,
                                        OrderStatus.delivered]:
            if validated_data['status'] == OrderStatus.delivered:
                validated_data['delivered_date'] = timezone.now()
            validated_data['is_send'] = True

        return super(OrderSerializer, self).update(instance, validated_data)


class PublishProductSerializer(serializers.Serializer):
    product_id = serializers.PrimaryKeyRelatedField(
        queryset=Product.objects.all())
    count = serializers.IntegerField()


class PublishOrderSerializer(serializers.Serializer):
    products = PublishProductSerializer(many=True)
    payment_type = EnumField(enum_type=PaymentType)

    def set_number(self, data: dict):
        data['order_number'] = Order.generate_number()
        return data

    def validate_product_and_set_restaurant_id(self, data: dict):
        restaurants = Restaurant.objects.filter(
            product__id__in=[product['product_id'] for product in
                             data.get('products')]
        ).distinct()
        if restaurants.count() > 1:
            raise ValidationError(
                'Orders cannot be placed in more than one restaurant.')
        restaurant = restaurants.get()
        data['restaurant_id'] = restaurant.id
        return data

    def custom_data(self, request):
        data = self.data
        data = self.set_number(data)
        data = self.validate_product_and_set_restaurant_id(data)
        data['customer_id'] = request.user.id
        return data


class AcceptOrDeclineSubscribedOrderSerializer(serializers.Serializer):
    accept_or_decline = serializers.BooleanField()
    order_number = serializers.CharField(max_length=128)


class SubscribedOrderSerializer(serializers.Serializer):
    products = PublishProductSerializer(many=True)
    payment_type = EnumField(enum_type=PaymentType)
    order_number = serializers.CharField(max_length=128)
    restaurant_id = serializers.PrimaryKeyRelatedField(
        queryset=Restaurant.objects.all())
    customer_id = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all())

    @atomic
    def generate(self, validated_data):
        order = Order(
            number=validated_data.get('order_number'),
            customer=validated_data.get('customer_id'),
            payment_type=validated_data.get('payment_type'),
            restaurant=validated_data.get('restaurant_id')
        )
        order.save()

        for product_data in validated_data.get('products'):
            product = product_data.get('product_id')
            for i in range(0, product_data.get('count')):
                OrderItem.objects.create(
                    product=product,
                    order=order,
                    price=product.active_price
                )
                product_stock = ProductStock.objects.get(product=product)
                product_stock.reduce_and_save()
        return order
