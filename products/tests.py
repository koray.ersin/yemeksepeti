from decimal import Decimal

from model_mommy import mommy

from base.tests import BaseApiTestCase
from products.models import Product, ProductStock, ProductPrice
from products.serializers import (ProductSerializer, ProductPriceSerializer,
                                  ProductStockSerializer)
from restaurants.models import Restaurant


class ProductTestCase(BaseApiTestCase):
    endpoint = "/api/v1/products/"

    def setUp(self):
        self.create_user()
        self.restaurant = mommy.make(Restaurant, user=self.user)

    def test_create_product(self):
        product_1 = mommy.prepare(Product, name="margarita pizza",
                                  restaurant=self.restaurant)
        product_1_serializer = ProductSerializer(product_1)
        serializer = ProductSerializer(
            data=product_1_serializer.data)
        self.assertTrue(serializer.is_valid())

        self.assertTrue(self.post_request_and_check_status(serializer.data))

    def test_update_product(self):
        product_1 = mommy.make(Product, name="margarita pizza",
                               restaurant=self.restaurant)

        data = {
            "name": "peperoni pizza"
        }
        self.assertTrue(
            self.patch_request_and_check_status(
                endpoint=self.endpoint + '{}/'.format(product_1.id),
                data=data
            )
        )

        product_1.refresh_from_db()
        self.assertEqual(product_1.name, data['name'])


class ProductPriceTestCase(BaseApiTestCase):
    endpoint = "/api/v1/product_prices/"

    def setUp(self):
        self.create_user()

        self.product = mommy.make(Product, name="margarita pizza")

    def test_create_product_price(self):
        self.assertIsNone(self.product.active_price)

        product_price_1 = mommy.prepare(ProductPrice, price=Decimal(20),
                                        product=self.product)
        product_price_1_serializer = ProductPriceSerializer(product_price_1)
        serializer = ProductPriceSerializer(
            data=product_price_1_serializer.data)
        self.assertTrue(serializer.is_valid())

        self.assertTrue(self.post_request_and_check_status(serializer.data))

        product_price_1 = ProductPrice.objects.get(product=self.product)
        self.assertIsNotNone(product_price_1.id)
        self.assertEqual(self.product.active_price, Decimal(20))

    def test_update_product_price(self):
        self.assertIsNone(self.product.active_price)

        product_price_1 = mommy.make(ProductPrice, price=Decimal(20),
                                     product=self.product)

        data = {
            "price": Decimal(40)
        }
        self.assertTrue(
            self.patch_request_and_check_status(
                endpoint=self.endpoint + '{}/'.format(product_price_1.id),
                data=data
            )
        )
        product_price_1.refresh_from_db()

        self.assertEqual(product_price_1.price, data['price'])
        self.assertEqual(self.product.active_price, data['price'])


class ProductStockTestCase(BaseApiTestCase):
    endpoint = "/api/v1/product_stocks/"

    def setUp(self):
        self.create_user()

        self.product = mommy.make(Product, name="margarita pizza")

    def test_create_product_stock(self):
        self.assertEqual(self.product.active_stock, 0)
        self.assertFalse(self.product.has_stock)

        product_stock_1 = mommy.prepare(ProductStock, stock=15,
                                        product=self.product)
        product_stock_1_serializer = ProductStockSerializer(product_stock_1)
        serializer = ProductStockSerializer(
            data=product_stock_1_serializer.data)
        self.assertTrue(serializer.is_valid())

        self.assertTrue(self.post_request_and_check_status(serializer.data))

        product_stock_1 = ProductStock.objects.get(product=self.product)
        self.assertIsNotNone(product_stock_1.id)
        self.assertEqual(self.product.active_stock, 15)
        self.assertTrue(self.product.has_stock)

    def test_update_product_stock(self):
        self.assertEqual(self.product.active_stock, 0)
        self.assertFalse(self.product.has_stock)

        product_stock_1 = mommy.make(ProductStock, stock=15,
                                     product=self.product)
        self.assertEqual(self.product.active_stock, 15)

        data = {
            "stock": 20
        }
        self.assertTrue(
            self.patch_request_and_check_status(
                endpoint=self.endpoint + '{}/'.format(product_stock_1.id),
                data=data
            )
        )
        product_stock_1.refresh_from_db()

        self.assertEqual(product_stock_1.stock, data['stock'])
        self.assertEqual(self.product.active_stock, data['stock'])
        self.assertTrue(self.product.has_stock)
