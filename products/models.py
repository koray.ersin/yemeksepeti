import uuid

from django.db import models

from base.models import StarterModel
from restaurants.models import Restaurant


class Product(StarterModel):
    name = models.CharField(max_length=255)
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    restaurant = models.ForeignKey(Restaurant)

    def __str__(self):
        return self.name

    @property
    def active_stock(self):
        try:
            return self.productstock.stock
        except ProductStock.DoesNotExist:
            return 0

    @property
    def active_price(self):
        try:
            return self.productprice.price
        except ProductPrice.DoesNotExist:
            return None

    @property
    def has_stock(self):
        return bool(self.active_stock)


class ProductStock(StarterModel):
    stock = models.IntegerField()
    product = models.OneToOneField(Product)

    def __str__(self):
        return '{} - {}'.format(self.product.name, self.stock)

    def reduce_and_save(self):
        if self.product.has_stock:
            self.stock -= 1
            self.save()


class ProductPrice(StarterModel):
    price = models.DecimalField(max_digits=12, decimal_places=2)
    product = models.OneToOneField(Product)

    def __str__(self):
        return '{} - {}'.format(self.product.name, self.price)
