from rest_framework import viewsets

from base.views import AdminOrReadonlyPermissionMixin
from products.models import Product, ProductStock, ProductPrice
from products.serializers import (ProductSerializer, ProductStockSerializer,
                                  ProductPriceSerializer)


class ProductViewSet(AdminOrReadonlyPermissionMixin, viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class ProductStockViewSet(AdminOrReadonlyPermissionMixin,
                          viewsets.ModelViewSet):
    queryset = ProductStock.objects.all()
    serializer_class = ProductStockSerializer


class ProductPriceViewSet(AdminOrReadonlyPermissionMixin,
                          viewsets.ModelViewSet):
    queryset = ProductPrice.objects.all()
    serializer_class = ProductPriceSerializer
