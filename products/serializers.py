from rest_framework import serializers

from products.models import Product, ProductStock, ProductPrice


class ProductStockSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductStock
        fields = ('pk', 'product', 'stock')


class ProductPriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductPrice
        fields = ('pk', 'product', 'price')


class ProductSerializer(serializers.ModelSerializer):
    productstock = ProductStockSerializer(read_only=True)
    productprice = ProductPriceSerializer(read_only=True)

    class Meta:
        model = Product
        fields = ('pk', 'created_date', 'modified_date', 'name', 'uuid',
                  'productstock', 'productprice', 'restaurant')
        read_only_fields = ('created_date', 'modified_date')
