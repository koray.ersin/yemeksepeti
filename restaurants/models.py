from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils import timezone

from base.models import StarterModel


class Restaurant(StarterModel):
    name = models.CharField(max_length=255)
    address = models.TextField()
    working_hours = JSONField(default=dict)
    user = models.OneToOneField(User)

    def __str__(self):
        return self.name

    @property
    def is_during_working_hour(self):
        now = timezone.now()
        week_day = str(now.weekday())
        try:
            selected_day = self.working_hours[week_day]

            return selected_day['start'] < now.hour < selected_day['end']
        except KeyError:
            return False
