import mock
from django.utils import timezone
from model_mommy import mommy

from base.tests import BaseApiTestCase
from restaurants.models import Restaurant
from restaurants.serializers import RestaurantSerializer


class RestaurantTestCase(BaseApiTestCase):
    endpoint = "/api/v1/restaurants/"

    def setUp(self):
        self.create_user()

    def test_create_restaurant(self):
        restaurant_1 = mommy.prepare(Restaurant, name="Pizza Store",
                                     address="lorem ipsum dolor sit amet.",
                                     user=self.user)
        restaurant_1_serializer = RestaurantSerializer(restaurant_1)
        serializer = RestaurantSerializer(
            data=restaurant_1_serializer.data)
        self.assertTrue(serializer.is_valid(raise_exception=True))
        self.assertTrue(self.post_request_and_check_status(serializer.data))

    def test_update_restaurant(self):
        restaurant_1 = mommy.make(Restaurant, name="Pizza Store",
                                  address="lorem ipsum dolor sit amet.",
                                  user=self.user)
        data = {
            "name": "Pizza Shop"
        }
        self.assertTrue(
            self.patch_request_and_check_status(
                endpoint=self.endpoint + '{}/'.format(restaurant_1.id),
                data=data
            )
        )
        restaurant_1.refresh_from_db()

        self.assertEqual(restaurant_1.name, data['name'])

    def test_restaurant_is_working_hour(self):
        restaurant_1 = mommy.prepare(
            Restaurant, name="Pizza Store",
            address="lorem ipsum dolor sit amet.",
            working_hours={
                "0": {"start": 8, "end": 23},
                "1": {"start": 8, "end": 23},
                "2": {"start": 8, "end": 23},
                "3": {"start": 8, "end": 23},
                "4": {"start": 8, "end": 23},
                "5": {"start": 12, "end": 20}
            },
            user=self.user
        )
        restaurant_1_serializer = RestaurantSerializer(restaurant_1)
        serializer = RestaurantSerializer(
            data=restaurant_1_serializer.data)
        self.assertTrue(serializer.is_valid())

        self.assertTrue(self.post_request_and_check_status(serializer.data))
        restaurant_1 = Restaurant.objects.get(name="Pizza Store")

        now = timezone.datetime(hour=10, year=2022, day=1, month=1)
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = now
            self.assertFalse(restaurant_1.is_during_working_hour)

        now = timezone.datetime(hour=10, year=2022, day=3, month=1)
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = now
            self.assertTrue(restaurant_1.is_during_working_hour)

        now = timezone.datetime(hour=10, year=2022, day=2, month=1)
        with mock.patch('django.utils.timezone.now') as mock_now:
            mock_now.return_value = now
            self.assertFalse(restaurant_1.is_during_working_hour)
