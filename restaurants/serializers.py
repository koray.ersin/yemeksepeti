from rest_framework import serializers

from restaurants.models import Restaurant


class RestaurantSerializer(serializers.ModelSerializer):
    working_hours = serializers.JSONField(default={})

    class Meta:
        model = Restaurant
        fields = ('pk', 'name', 'address', 'working_hours', 'user')
