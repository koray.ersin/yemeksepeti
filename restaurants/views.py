from rest_framework import viewsets

from base.views import AdminOrReadonlyPermissionMixin
from restaurants.models import Restaurant
from restaurants.serializers import RestaurantSerializer


class RestaurantViewSet(AdminOrReadonlyPermissionMixin, viewsets.ModelViewSet):
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer
